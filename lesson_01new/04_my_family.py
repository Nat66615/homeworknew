#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Создайте списки:

# моя семья (минимум 3 элемента, есть еще дедушки и бабушки, если что)
my_family = ['father', 'daughter', 'niece', 'sister']
# список списков приблизителного роста членов вашей семьи
my_family_height = [
    ['Yuri', 179],
    ['Alexandra', 169],
    ['Galina', 168],
    ['Helena', 159]
]
# Выведите на консоль рост отца
index_father = my_family.index('father')
print("Рост отца: " + str(my_family_height[index_father][1]))
# Выведите на консоль общий рост вашей семьи как сумму ростов всех членов
sum_height = (my_family_height[0][1]
              + my_family_height[1][1]
              + my_family_height[2][1]
              + my_family_height[3][1]
              )
print("Общий рост семьи: " + str(sum_height))

