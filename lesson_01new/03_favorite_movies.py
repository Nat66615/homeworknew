#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Есть строка с перечислением фильмов

my_favorite_movies = 'Терминатор, Пятый элемент, Аватар, Чужие, Назад в будущее'
index1 = (my_favorite_movies.find(","))
print('Первый фильм: ' + (my_favorite_movies[:index1]))
index_last = (my_favorite_movies.rfind(","))
print('Последний фильм: ' + (my_favorite_movies[index_last + 2:]))
index2 = (my_favorite_movies.find(',', index1 + 1))
print('Второй фильм: ' + (my_favorite_movies[index1 + 2:index2]))
index_penult = (my_favorite_movies.rfind(',', 0, index_last - 1))
print('Второй с конца фильм: ' + (my_favorite_movies[index_penult + 2:index_last]))

# зачет
