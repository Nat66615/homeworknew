#!/usr/bin/env python
# -*- coding: utf-8 -*-

# есть список животных в зоопарке

zoo = ['lion', 'kangaroo', 'elephant', 'monkey', ]

# посадите медведя (bear) во 2-ю клетку
zoo.insert(1, 'bear')
print(zoo)

# добавьте птиц в последние клетки
birds = ['rooster', 'ostrich', 'lark', ]
zoo.extend(birds)
print(zoo)

# уберите слона
zoo.remove('elephant')
print(zoo)

# выведите на консоль в какой клетке сидит обезьяна (monkey)
# и жаворонок (lark)
index_monkey = zoo.index('monkey') + 1
index_lark = zoo.index('lark') + 1
print("Обезьяна в клетке ", index_monkey)
print("Жаворонок в клетке ", index_lark)

# зачет
